var currentLetter = 'O'
    
    function clickCell(num){
      if( isWinner() === true || isCatsGame()){
        resetGame();
        return;
      }

      if( document.getElementById('cell' + num).innerHTML === "X"
          ||
          document.getElementById('cell' + num).innerHTML === "O" ){
        return;
      }
      

      if( currentLetter === "O" ){
        currentLetter = "X";
      }
      else if( currentLetter === "X" ){
        currentLetter = "O";
      }
      else{
        alert(' issue with logic');
      }

      document.getElementById('cell' + num).innerHTML = currentLetter;

      if( isWinner() === true){
        alert(currentLetter + ' is the winner!');
      } else if (isCatsGame()) {
          alert(' Cats Game! ');
      }
    }

    function isCatsGame () {
      var counter = 0;
      for (var i = 0; i < 9; i++) {
        if (document.getElementById('cell' + i).innerHTML === 'X' || 
          document.getElementById('cell' + i).innerHTML === 'O') {
            counter++;
        }
      };
      if (counter === 9) {
        return true;
      };
      return false;
    }

    function isWinner(){
      var winningCombos = [
        [0,1,2],
        [3,4,5],
        [6,7,8],
        [0,3,6],
        [1,4,7],
        [2,5,8],
        [0,4,8],
        [6,4,2]
      ];

      for(var i = 0 ; i < winningCombos.length; i++ ){
        var combo = winningCombos[i];
        if(
          document.getElementById('cell' + combo[0]).innerHTML === currentLetter
            &&
          document.getElementById('cell' + combo[1]).innerHTML === currentLetter
            &&
          document.getElementById('cell' + combo[2]).innerHTML === currentLetter
            )
        return true;
        
      }

      return false;
    }

  function resetGame(){
    for (var i = 0; i < 9; i++){
      document.getElementById('cell' + i).innerHTML = "";
    }
  }